﻿#include <iostream>
#include <string>

using std::cout;
using std::string;
using std::endl;

int main()
{
    setlocale ( 0 , "" );
        
    string name { "Приймак Максим Александрович" };    

    cout << "Строка: "              << name                     << endl;
    cout << "Длина строки: "        << name.size()              << endl;
    cout << "Первый символ: "       << name[0]                  << endl;
    cout << "Последний символ: "    << name[name.length()-1]    << endl;

    return 0;
}
